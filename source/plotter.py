import numpy as np 
import matplotlib.pyplot as plt 
from mpl_toolkits.mplot3d.axes3d import Axes3D

def plotVectorField(xV,yV):
	fig = plt.figure(figsize=(10,18))
	ax1 = fig.add_subplot(211)
	ax1.quiver(xV, yV)
	ax1.set_title('Figure')
 
#def plotScalarField(P):
#    fig = plt.figure(figsize=(10,18))
#    ax = fig.add_subplot(1, 2, 2, projection='3d')
#    ax.plot_surface(P, rstride=1, cstride=1, cmap=cm.coolwarm, linewidth=0, antialiased=False)
     
def plotFunction(f,S,E,width):
    plt.figure(figsize=(10,18))
    plt.subplot(211)
    t = np.arange(S, E, width)    
    plt.plot(t, f)