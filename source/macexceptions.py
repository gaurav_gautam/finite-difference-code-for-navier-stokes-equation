class InconsistentMesh(Exception):
    def __init__(self, msg):
        self.msg = msg

class InvalidParameter(Exception):
    def __init__(self, msg):
        self.msg = msg
        