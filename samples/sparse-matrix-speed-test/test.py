
from mac import *
import numpy as np
import scipy as sp
import time

tub = Mac()
tub.setMeshBeginX(0.0)
tub.setMeshEndX(1)
tub.setMeshBeginY(0.0)
tub.setMeshEndY(1)
tub.setMeshWidthX(0.1)
tub.setMeshWidthY(0.1)
tub.setTimeBegin(0.0)
tub.setTimeEnd(1.5)
tub.setTimeStep(0.0007)
tub.setViscosity(0)
tub.setDensity(1)
tub.setArtificialCompressiblityFactor(0.1)
tub.initializeMesh()
tub.printMeshParameters()

x = np.random.rand(tub.xV.size,1)
t = np.empty([100,1])
A = sp.sparse.csr_matrix(tub.TD1xx)#,(tub.xN-1, tub.xN-1))
print A.astype
for i in range(0,100) :
    start = time.time()
    b = sp.sparse.linalg.lsqr(A,x)[0]    
    b = np.reshape(b,[b.size,1])
    b.flatten()
    end = time.time()
    t[i] = (end-start)
print np.sum(t)/100

A = ty.dia_matrix(tub.TD1xy)#,(tub.xN, tub.xN))
print A.astype
for i in range(0,100) :
    start = time.time()
    b = A.dot(x)
    end = time.time()
    t[i] = (end-start)
print np.sum(t)/100

A = ty.dia_matrix(tub.TD1yx)#,(tub.xN-1, tub.xN-1))
print A.astype
for i in range(0,100) :
    start = time.time()
    b = A.dot(x)
    end = time.time()
    t[i] = (end-start)
print np.sum(t)/100

A = ty.dia_matrix(tub.TD1yy)#,(tub.xN, tub.xN))
print A.astype
for i in range(0,100) :
    start = time.time()
    b = A.dot(x)
    end = time.time()
    t[i] = (end-start)
print np.sum(t)/100