from mac import *
import plotter as plt
import numpy as np
import scipy as sp
import time

tub = Mac()
tub.setMeshBeginX(0.0)
tub.setMeshEndX(2*np.pi)
tub.setMeshBeginY(0.0)
tub.setMeshEndY(2*np.pi)
tub.setMeshWidthX(0.1)
tub.setMeshWidthY(0.1)
tub.setTimeBegin(0.0)
tub.setTimeEnd(.1)
tub.setTimeStep(0.001)
tub.setArtificialCompressiblityFactor(0.1)
tub.setDensity(1.)
tub.setViscosity(0.)
start = time.time()
tub.initializeMesh()
end = time.time()
print '>initializeMesh()|Time:',end-start
tub.printMeshParameters()
plt.plotVectorField(tub.getVelocityX(),tub.getVelocityY())
tub.advanceBy(tub.tN)
plt.plotVectorField(tub.getVelocityX(),tub.getVelocityY())

plt.plotFunction(tub.energy,tub.tB,tub.tE,tub.dt)
e = tub.energy